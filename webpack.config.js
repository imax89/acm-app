const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path');
const webpack = require('webpack');
module.exports = {
	entry: "./src/App.jsx",
	
	output: {
		filename: "bundle.js",
		path: path.resolve(__dirname, "App")
	},
	
	module: {
		rules: [
			{
				test: /\.(sass|scss)$/,
				use: ExtractTextPlugin.extract({
					use: ["css-loader","sass-loader"] /* convert SASS files to CSS ones */
				})
			},
			{
				test: /\.jsx?$/,
				exclude: /node_modules/,
				use: [
					{
						loader: 'babel-loader' // creates style nodes from JS strings
					}
				]
			}
		]
	},
	devtool: "nosources-source-map",
	plugins: [
		new ExtractTextPlugin('./bundle.css'),
		//new webpack.DefinePlugin({
		//	'process.env':{
		//		'NODE_ENV': JSON.stringify('production')
		//	}
		//})
	],
	resolve: {
		extensions: ['.js','.jsx', '.sass']
	}
	
};