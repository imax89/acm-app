#ACM-App
####AllenCarrMoscow interactive calendar app

To install this project, install node modules with NPM 
```javascript
npm i 
```

run "build":
```javascript
npm run build
```

and "start" script as well
 
```javascript
npm run start
```
