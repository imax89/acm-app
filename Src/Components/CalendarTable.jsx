import React from 'react';
import CalendarCell from './CalendarCell';

function CalendarTable (props) {
	
	function getDay(date) { /* get the number of the day of the week (0 - Monday, 6 - Sunday) */
		let day = date.getDay();
		if (day === 0) day = 7;
		return day - 1;
	}
	
	function daysInMonth(month,year) {
		return new Date(year, month, 0).getDate();
	}
	
	let mon     = props.monthNumber;
	let year    = props.year;
	let d       = new Date(year, mon);
	let nextMonthDayNumber = 1;
	let prevYear = props.year;
	if (props.monthNumber===0) {
		prevYear--;
	}
	
	let daysArray = []; /* This array is for filling with days which will be presented in the calendar */
	
	let prevMonthDays   = daysInMonth(props.monthNumber, prevYear);
	let dayShortage     = 0; /* how many days belong to the previous month*/
	
	for (let i = 0; i < getDay(d); i++) {
		dayShortage++;
	}
	let firstCalendarDay = prevMonthDays - dayShortage + 1;
	
	/* Let's fill first calendar row with the days of the previous week if they exist */
	for (let i = 0; i < getDay(d); i++) {
		daysArray.push({type: "prevMonth", dn: firstCalendarDay});
		firstCalendarDay++;
	}
	
	/* Filling our calendar with the days of the current week if they exist */
	while (d.getMonth() === mon) {
		daysArray.push({type: "curMonth", dn: d.getDate()});
		d.setDate(d.getDate() + 1);
	}
	
	/* Finally, here's filling the last calendar row with the days of the next week if they exist */
	if (getDay(d) !== 0) {
		for (let i = getDay(d); i < 7; i++) {
			daysArray.push({type: "nextMonth", dn: nextMonthDayNumber});
			nextMonthDayNumber++;
		}
	}
	
	return (
		<div>
			<div className="calendarHead">
				<div className="day-cell">Пн</div>
				<div className="day-cell">Вт</div>
				<div className="day-cell">Ср</div>
				<div className="day-cell">Чт</div>
				<div className="day-cell">Пт</div>
				<div className="day-cell">Сб</div>
				<div className="day-cell">Вс</div>
			</div>
			<div className="calendarBody">
			{daysArray.map((o,i)=> {
				if (o.type === 'prevMonth') return (<CalendarCell dayNumber={o.dn} key={i} />);
				if (o.type === 'curMonth' ) return (<CalendarCell dayNumber={o.dn} key={i} />);
				if (o.type === 'nextMonth') return (<CalendarCell dayNumber={o.dn} key={i} />);
			})}
			</div>
		</div>
	)
}

export default CalendarTable;