import React from 'react';

function CalendarCell (props) {
	
	
	return (
		<div className="day-cell">
			<span>{props.dayNumber}</span>
		</div>
	)
}

export default CalendarCell;