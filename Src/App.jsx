import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import CalendarTable from './Components/CalendarTable';
import './bundle.sass';

/* Input data */
const DATA = schedule_table;

class App extends Component {
	constructor (props) {
		super(props);
		this.monthDown  = this.monthDown.bind(this);
		this.monthUp    = this.monthUp.bind(this);
		
		this.state      = {
			currentMonthNumber: (new Date).getMonth(),
			currentYear: (new Date).getFullYear(),
			currentMonthName: ()=>{
				let name = "";
				switch (this.state.currentMonthNumber) {
					case 0:     name = "Январь";        break;
					case 1:     name = "Февраль";       break;
					case 2:     name = "Март";          break;
					case 3:     name = "Апрель";        break;
					case 4:     name = "Май";           break;
					case 5:     name = "Июнь";          break;
					case 6:     name = "Июль";          break;
					case 7:     name = "Август";        break;
					case 8:     name = "Сентябрь";      break;
					case 9:     name = "Октябрь";       break;
					case 10:    name = "Ноябрь";        break;
					case 11:    name = "Декабрь";       break;
				}
				return name;
			},
		};
	}
	
	monthDown(e) {
		e.preventDefault();
		this.state.currentMonthNumber--;
		if (this.state.currentMonthNumber < 0) {
			this.state.currentMonthNumber = 11;
			this.state.currentYear = this.state.currentYear-1;
		}
		this.setState({currentMonthNumber: this.state.currentMonthNumber});
	}
	
	monthUp(e) {
		e.preventDefault();
		this.state.currentMonthNumber++;
		if (this.state.currentMonthNumber > 11) {
			this.state.currentMonthNumber = 0;
			this.state.currentYear = this.state.currentYear+1;
		}
		this.setState({
			currentMonthNumber: this.state.currentMonthNumber,
			currentYear: this.state.currentYear,
		});
	};
	
	render(){
		
		return(
			<div id="App">
				<h2>{this.state.currentMonthName()}, {this.state.currentYear}</h2>
				<a href="#" onClick={this.monthDown}>Уменьшить месяц</a>&nbsp;
				<a href="#" onClick={this.monthUp}>Увеличить месяц</a>
				
				<div className="calendar-wrap">
					<CalendarTable
						monthNumber={this.state.currentMonthNumber}
						monthName={this.state.currentMonthName()}
						year={this.state.currentYear}
					/>
				</div>
				{/*{DATA.map((i,n)=>(*/}
					{/*<div key={n}>{i.data}</div>*/}
				{/*))}*/}
			</div>
		)
	}
}

ReactDOM.render(<App />, document.getElementById('AppMountPoint'));